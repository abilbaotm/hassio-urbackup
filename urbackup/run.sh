#!/bin/bash
# shellcheck shell=dash

# Extract config data
CONFIG_PATH=/data/options.json
URBACKUP_SERVER_NAME=$(jq --raw-output ".URBACKUP_SERVER_NAME" "$CONFIG_PATH")
URBACKUP_CLIENT_NAME=$(jq --raw-output ".URBACKUP_CLIENT_NAME" "$CONFIG_PATH")
URBACKUP_CLIENT_AUTHKEY=$(jq --raw-output ".URBACKUP_CLIENT_AUTHKEY" "$CONFIG_PATH")


# FROM https://github.com/uroni/urbackup-client-docker/blob/latest/entrypoint.sh
set -e

if [[ "$URBACKUP_SERVER_NAME" == "" ]]
then
	echo "Please specify UrBackup server DNS name/IP via environment variable URBACKUP_SERVER_NAME"
	exit 1
fi

if [[ "$URBACKUP_CLIENT_NAME" == "" ]]
then
	echo "Please specify UrBackup client name via environment variable URBACKUP_CLIENT_NAME"
	exit 1
fi

if [[ "$URBACKUP_CLIENT_AUTHKEY" == "" ]]
then
	echo "Please specify UrBackup client authentication key via environment variable URBACKUP_CLIENT_AUTHKEY"
	exit 1
fi

set -e
setup() {
	urbackupclientctl wait-for-backend

	IFS=':'
	for dir in $URBACKUP_BACKUP_VOLUMES
	do
		echo "Backing up volume $dir"
		urbackupclientctl add -d "$dir"
	done
	unset IFS

	urbackupclientctl set-settings \
		-k internet_mode_enabled -v true \
		-k internet_server -v "$URBACKUP_SERVER_NAME" \
		-k internet_server_port -v "$URBACKUP_SERVER_PORT" \
		-k computername -v "$URBACKUP_CLIENT_NAME" \
		-k internet_authkey -v "$URBACKUP_CLIENT_AUTHKEY"

	if [[ "$URBACKUP_SERVER_PROXY" != "" ]]
	then
		urbackupclientctl set-settings \
			-k internet_server_proxy -v "$URBACKUP_SERVER_PROXY"
	fi

	if [[ "$URBACKUP_CLIENT_MYSQL_BACKUP" == "1" ]]
	then
		cat << EOF > /usr/local/etc/urbackup/mariadbdump.conf
#Enable MariaDB dump backup
MARIADB_DUMP_ENABLED=1
#Backup user account
MARIADB_BACKUP_USER=$MYSQL_USER
#Backup user password
MARIADB_BACKUP_PASSWORD=$MYSQL_PASSWORD
MARIADB_DUMP="mysqldump --host=$MYSQL_HOST --no-tablespaces"
EOF
	fi
}

setup &
exec /usr/local/sbin/urbackupclientbackend "$@"
